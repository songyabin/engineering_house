# 工程之家

#### 项目介绍
为豫工院的师生提供方便快捷的生活服务App！

#### 软件架构
软件架构说明


#### 安装教程

1. npm i 或 cnpm i 安装项目依赖(没有安装自行百度)
2. npm run mock 启动数据模拟服务
3. npm start 启动项目
4. 推荐在vscode中打开，因为其他的我也不会

#### 使用说明

1. 默认开启端口8080，后台数据在3000端口，使用了代理。
2. 浏览器中F12在移动端模式下查看

#### 参与贡献

就老子一个人


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
7. test add
####待处理问题
1. 配色:
main   #0069a0;
light  #5097d1
dark   #003f71  white

2. material Ui
组件内嵌套的样式修改很麻烦，尤其是在类似悬停，点击这种特殊状态需要改变样式时及其麻烦
