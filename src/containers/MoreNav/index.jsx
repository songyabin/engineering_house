import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
// import style from './moren.module.less'

import HeaderWithBack from '../../components/HeaderWithBack'
class MoreNav extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
	}
	render() {
		return (
			<div>
				<HeaderWithBack title="更多功能" />
			</div>
		)
	}
}
export default MoreNav