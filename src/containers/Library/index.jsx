import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
// import style from './libra.module.less'
import HeaderWithBack from '../../components/HeaderWithBack'
class Library extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
	}
	render() {
		return (
			<div>
				<HeaderWithBack title="图书馆" />
			</div>
		)
	}
}
export default Library