import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
// import style from './class.module.less'
import HeaderWithBack from '../../components/HeaderWithBack'
class ClassSchedule extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
	}
	render() {
		return (
			<div>
				<HeaderWithBack title="课程表" />
			</div>
		)
	}
}
export default ClassSchedule