import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
// import style from './user.module.less'
import HeaderWithBack from '../../components/HeaderWithBack'
class User extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
	}
	render() {
		return (
			<div>
				<HeaderWithBack title="用户中心" />
			</div>
		)
	}
}
export default User