import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'

import SearchHeader from '../../components/SearchHeader'

import style from './searc.module.less'

import BottomNavigation from '../../components/BottomNavigation'
// import BottomNavigation from '@material-ui/core/BottomNavigationAction'
// import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
// import RestoreIcon from '@material-ui/icons/Restore';
// import FavoriteIcon from '@material-ui/icons/Favorite';
// import LocationOnIcon from '@material-ui/icons/LocationOn';
// import Card from '@material-ui/core/Card';
class Search extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
	}
	render() {
		return (
			<div className={style.wraper}>
				<SearchHeader />
				<BottomNavigation />
			</div>
		)
	}
}
export default Search