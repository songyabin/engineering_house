import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import HeaderWithBack from '../../components/HeaderWithBack'
class ActivityDetail extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
	}
	render() {
		return (
			<div>
				<HeaderWithBack title="活动详情" />
			</div>
		)
	}
}
export default ActivityDetail