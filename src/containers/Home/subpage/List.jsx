import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import { getListData } from '../../../fetch/home/home'
import ListComponent from '../../../components/List'
import LoadMore from '../../../components/LoadMore'

import ContentLoading from '../../../components/ContentLoading'
// import style from './style.moudle.less'


// 列表模块和加载更多模块在 首页--搜索页--评论页--？ 都会用到，后端返回数据格式统一，考虑封装起来，传参数调用，在列表下分别写各自的 Item组件处理列表项数据即可
class List extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
		this.state = {
			data: [],
			hasMore: false,
			isLoadngMore: false,
			page_no: 1
		}
	}
	render() {
		return (
			<div>
				{
					this.state.data.length
						? <ListComponent data={this.state.data} />
						: <ContentLoading />
				}
				{
					this.state.hasMore
						? <LoadMore isLoadingMore={this.state.isLoadingMore} loadMoreFn={this.loadMoreData.bind(this)} />
						: <div>——已经到底了哦——</div>
				}

			</div>
		)
	}
	componentDidMount() {
		this.loadFirstPageData();
	}

	// 获取首屏数据
	loadFirstPageData() {
		// const cityName = this.props.cityName;
		const result = getListData(1);
		this.resultHandle(result);
	}
	// 加载更多数据
	loadMoreData() {
		this.setState({
			isLoadingMore: true
		})
		// const cityName = this.props.cityName;
		const page_no = this.state.page_no
		const result = getListData(page_no);
		this.resultHandle(result);

		this.setState({
			page_no: page_no + 1,
			isLoadingMore: false
		})
	}
	// 数据处理，公共代码提取
	resultHandle(result) {
		result.then(res => {
			return res.json();
		}).then(json => {
			const hasMore = json.hasMore
			const data = json.data
			// 将处理好的数据存入state
			this.setState({
				hasMore,
				// 将获取到的数据添加到当前已获取数据之后
				data: this.state.data.concat(data)
			})
		})
	}
}
export default List