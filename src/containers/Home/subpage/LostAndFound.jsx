import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import { getLostAndFoundData } from '../../../fetch/home/home'
import LostAndFoundComponent from '../../../components/LostAndFound'
// import style from './style.moudle.less'
class LostAndFound extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
		this.state = {
			data: []
		}
	}
	render() {
		return (
			<div>
				{
					this.state.data.length
						? <LostAndFoundComponent data={this.state.data} />
						: <div>加载中...</div>
				}
			</div>
		)
	}

	componentDidMount() {
		const result = getLostAndFoundData();
		result.then((res) => {
			return res.json()
		}).then((json) => {
			const data = json;
			if (data.length) {
				this.setState({
					data
				})
			}
		})
	}
}
export default LostAndFound