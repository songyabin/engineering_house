import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import HotActivitiesComponent from '../../../components/HotActivities'
import { getActivitiesData } from '../../../fetch/home/home'

class HotActivities extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
		this.state = {
			data: []
		}
	}
	render() {
		return (
			<div>
				{
					this.state.data.length
						? <HotActivitiesComponent data={this.state.data} />
						: <div>加载中...</div>
				}
			</div>
		)
	}
	componentDidMount() {
		const result = getActivitiesData();
		result.then((res) => {
			return res.json()
		}).then((json) => {
			const data = json;
			if (data.length) {
				this.setState({
					data
				})
			}
		})
	}
}
export default HotActivities