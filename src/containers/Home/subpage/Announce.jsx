import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import AnnounceComponent from '../../../components/Announcement'
import { getAnnounceData } from '../../../fetch/home/home'
import ContentLoading from '../../../components/ContentLoading'
class Announce extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
		this.state = {
			data: []
		}
	}
	render() {
		return (
			<div>
				{
					this.state.data.length
						? <AnnounceComponent data={this.state.data} />
						: <ContentLoading />
				}
			</div>
		)
	}
	componentDidMount() {
		const result = getAnnounceData();
		result.then((res) => {
			return res.json()
		}).then((json) => {
			const data = json;
			if (data.length) {
				this.setState({
					data
				})
			}
		})
	}
}
export default Announce