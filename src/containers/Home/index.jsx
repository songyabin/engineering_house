import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'

import style from './home.module.less'
import Category from '../../components/Category'
import Title from '../../components/Title'
import HomeNav from '../../components/HomeNav'
import Announcement from './subpage/Announce'
import Appbar from '../../components/Appbar'
import HotActivities from './subpage/HotActivities'
class Home extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
	}
	render() {
		return (
			<div className={style.homebody}>
				<Appbar />
				<Category />
				<HomeNav />
				<Title title="公告" showMore={false} linkto="/announcement" />
				<Announcement />
				<Title title="热门活动" showMore={true} linkto="/hotActivities" />
				<HotActivities />
				<div className={style.footer}>----我也是有底线的----</div>
			</div>
		)
	}
}
export default Home