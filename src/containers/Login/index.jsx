import React from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import UserIcon from '@material-ui/icons/AccountCircle';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = theme => ({
	paper: {
		marginTop: 50,
		padding: "0 15px",
		display: 'flex',
		boxShadow: "none",
		flexDirection: 'column',
		alignItems: 'center',
	},
	avatar: {
		margin: 15,
		backgroundColor: "#0069a0",
	},
	form: {
		width: '100%', // Fix IE11 issue.
		marginTop: 15,
	},
	submit: {
		padding: '10px 0',
		fontSize: '16px',
		marginTop: 20,
		backgroundColor: "#0069a0",
	},
});


function SignIn(props) {
	const { classes } = props;


	return (
		<Paper className={classes.paper}>
			<Avatar className={classes.avatar}>
				<UserIcon />
			</Avatar>
			<Typography component="h1" variant="title">
				登录
          			</Typography>
			<form className={classes.form}>
				<FormControl margin="normal" fullWidth>
					<InputLabel htmlFor="email">学号*</InputLabel>
					<Input id="email" name="email" autoComplete="off" autoFocus />
				</FormControl>
				<FormControl margin="normal" fullWidth>
					<InputLabel htmlFor="password">密码*(教务系统密码)</InputLabel>
					<Input
						name="password"
						type="password"
						id="password"
						autoComplete="current-password"
					/>
				</FormControl>
				<Button
					type="submit"
					fullWidth
					variant="contained"
					color="primary"
					className={classes.submit}
					onClick={clickHandle.bind(this)}
				>
					登录
            			</Button>
			</form>
		</Paper>
	);

	function clickHandle() {
		console.log(11111)
	}

}

SignIn.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SignIn)