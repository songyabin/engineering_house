import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
// import style from './hotac.moudle.less'
import HeaderWithBack from '../../components/HeaderWithBack'
class HotActivities extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
	}
	render() {
		return (
			<div>
				<HeaderWithBack title="热门活动" />
			</div>
		)
	}
}
export default HotActivities