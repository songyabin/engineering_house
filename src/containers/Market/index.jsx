import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import HeaderWithBack from '../../components/HeaderWithBack'
// import style from './smile.module.less'
class Market extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
	}
	render() {
		return (
			<div>
				<HeaderWithBack title="二手市场" />
			</div>
		)
	}
}
export default Market