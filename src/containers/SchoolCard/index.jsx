import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'

import HeaderWithBack from '../../components/HeaderWithBack'
// import style from './schoo.module.less'
class SchoolCard extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
	}
	render() {
		return <div>
			<HeaderWithBack title="校园卡" />
		</div>
	}
}
export default SchoolCard