import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
// import style from './organ.module.less'
import HeaderWithBack from '../../components/HeaderWithBack'
class Organization extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
	}
	render() {
		return (
			<div>
				<HeaderWithBack title="社团" />
			</div>
		)
	}
}
export default Organization