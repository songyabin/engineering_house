import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import { withRouter } from 'react-router-dom';
import Login from './Login'
import BottomNavigation from '../components/BottomNavigation'
class App extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
		this.state = {
			initDone: true
		}
	}
	render() {

		return (

			<div className="container" style={{ paddingBottom: "55px" }}>
				{/* 从本地缓存和redux中获取登录信息，如果已登录则正常显示，否则跳转到登陆页 */}
				{
					this.state.initDone
						? this.props.children
						: <Login />
				}
				<BottomNavigation />
			</div>
		)
	}
}

// 由于这是所有页面入口且使用redux，导出时需要加一层withRouter,否则会忽略路由Link
export default withRouter(App) 