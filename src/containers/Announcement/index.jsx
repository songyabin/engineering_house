import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import HeaderWithBack from '../../components/HeaderWithBack'
class Announcement extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
	}
	render() {
		return (
			<div>
				<HeaderWithBack title="公告详情" />
			</div>
		)
	}
}
export default Announcement