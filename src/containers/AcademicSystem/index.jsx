import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
// import style from './acade.module.less'

import HeaderWithBack from '../../components/HeaderWithBack'
class AcademicSystem extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
	}
	render() {
		return (
			<div>
				<HeaderWithBack title="教务系统" />
			</div>
		)
	}
}
export default AcademicSystem