var Koa = require('koa');
var Router = require('koa-router');
// 这个依赖，能够使koa正常解析请求参数，非常重要！！！,通过ctx.request.body获取
var bodyparser = require('koa-bodyparser');
var app = new Koa();
var router = new Router();


// 首页-公告
var homeAnnounceData = require('./home/announcement.js')
router.get('/api/homeAnnounce', (ctx, next) => {
	ctx.body = homeAnnounceData;
})

// 首页-失物招领
var homeLostAndFoundData = require('./home/lostandfound.js')
router.get('/api/homeLostAndFound', (ctx, next) => {
	ctx.body = homeLostAndFoundData;
})

// 首页-热门活动
var homeActivitiesData = require('./home/activities.js')
router.get('/api/homeActivities', (ctx, next) => {
	ctx.body = homeActivitiesData
})














// 首页-为你推荐
var homeListData = require('./home/list.js')
router.get('/api/homeList/:page_no', (ctx, next) => {
	// 参数处理
	const params = ctx.params;
	console.log(params)
	// const city = params.city;
	const page_no = params.page_no;
	// 测试数据，模拟后台数据只有10页，数据处理的工作应放在后端处理
	if (page_no > 10) {
		homeListData.hasMore = false;
	}
	else {
		homeListData.hasMore = true;
	}

	if (homeListData.hasMore) {
		ctx.body = homeListData;
	} else {
		ctx.body = JSON.stringify('Not More');
	}
})


// // 详情--商家信息
// var detailData = require('./detail/info.js')
// router.get('/api/detail/info/:id', (ctx, next) => {
// 	ctx.body = detailData;
// })

// // 详情--商家评论
// var commentlData = require('./detail/comment.js')
// router.get('/api/detail/comment/:page_no/:id', (ctx, next) => {
// 	ctx.body = commentlData;
// })











router.post('/api/post/post', (ctx, next) => {
	console.log('=================Post=======================')
	ctx.body = JSON.stringify(ctx.request.body);
});

app.use(bodyparser(
	{
		onerror: function (err, ctx) {
			ctx.throw('body parse error', 422);
		}
	}))
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(3000);