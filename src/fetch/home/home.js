import { get } from '../get'

export function getAnnounceData() {
	const result = get('/api/homeAnnounce')
	return result
}

export function getLostAndFoundData() {
	const result = get('/api/homeLostAndFound')
	return result
}

export function getActivitiesData() {
	const result = get('/api/homeActivities')
	return result
}

export function getListData(page_no) {
	const result = get('/api/homeList/' + encodeURIComponent(page_no));
	return result
}