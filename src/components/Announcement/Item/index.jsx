import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import { Link } from 'react-router-dom'
import style from './item.module.less'


class Item extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
	}
	render() {
		const data = this.props.data;
		return (
			<div>
				<Link to={"/announcement/" + data.id}>
					<div className={style.container + " cf"}>
						<h3 className="fl">{data.title}</h3>
						<span className="fr">{data.publish_time}</span>
					</div>
				</Link>
			</div>
		)
	}
}
export default Item