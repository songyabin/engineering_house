import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import { Link } from 'react-router-dom'
import style from './homen.module.less'
class HomeNav extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
	}
	render() {
		return (
			<div className={style.wraper}>
				<ul className="cf">
					<Link to="/schoolCard">
						<li>
							<div className={style.icon_container}>
								<i className="fa fa-credit-card fa-lg" style={{ color: "#5097d1" }}></i>
							</div>
							<h3>校园卡</h3>
						</li>
					</Link>
					<Link to="/library">
						<li>
							<div className={style.icon_container}>
								<i className="fa fa-building fa-lg" style={{ color: "#5097d1" }}></i>
							</div>
							<h3>图书馆</h3>
						</li>
					</Link>
					<Link to="/academicSystem">
						<li>
							<div className={style.icon_container}>
								<i className="fa fa-server fa-lg" style={{ color: "#5097d1" }}></i>
							</div>
							<h3>教务系统</h3>
						</li>
					</Link>
					<Link to="/classSchedule">
						<li>
							<div className={style.icon_container}>
								<i className="fa fa-table fa-lg" style={{ color: "#5097d1" }}></i>
							</div>
							<h3>课程表</h3></li>
					</Link>
					<Link to="/postgraduate">
						<li><div className={style.icon_container}>
							<i className="fa fa-graduation-cap fa-lg" style={{ color: "#5097d1" }}></i>
						</div>
							<h3>失物招领</h3>
						</li>
					</Link>
					<Link to="/organization">
						<li>
							<div className={style.icon_container}>
								<i className="fa fa-group fa-lg" style={{ color: "#5097d1" }}></i>
							</div>
							<h3>社团</h3>
						</li>
					</Link>
					<Link to="/market">
						<li>
							<div className={style.icon_container}>
								<i className="fa fa-handshake-o fa-lg" style={{ color: "#5097d1" }}></i>
							</div>
							<h3>二手市场</h3>
						</li>
					</Link>
					<Link to="/moreNav">
						<li>
							<div className={style.icon_container}>
								<i className="fa fa-plus-square-o fa-lg" style={{ color: "#5097d1" }}></i>
							</div>
							<h3>更多</h3>
						</li>
					</Link>
				</ul>
			</div>
		)
	}
}
export default HomeNav