import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import CreditCardIcon from '@material-ui/icons/CreditCard';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import AccountBalanceIcon from '@material-ui/icons/AccountBalance';
import UserIcon from '@material-ui/icons/AccountCircle';

import history from "../../untils/Hashhistory"

const theme = createMuiTheme({
	overrides: {
		MuiBottomNavigationAction: {
			selected: {
				// color: 'red',
			}
		}
	}
})

const styles = {
	root: {
		width: "100%",
		height: 55,
		position: "fixed",
		bottom: 0,
		left: 0,
		zIndex: 1,
		borderTop: "1px solid #ccc"
	},
};

class SimpleBottomNavigation extends React.Component {
	state = {
		value: 0,
	};

	handleChange = (event, value) => {
		this.setState({ value });
	};

	clickHandle(e) {
		const router = e.currentTarget.getAttribute('router')
		history.push(router)
	}

	render() {
		const { classes } = this.props;
		const { value } = this.state;

		return (
			<MuiThemeProvider theme={theme}>
				<BottomNavigation
					value={value}
					onChange={this.handleChange}
					showLabels
					classes={classes}
				>
					<BottomNavigationAction
						label="首页"
						icon={<AccountBalanceIcon />}
						router="/home"
						onClick={this.clickHandle.bind(this)}
					/>
					<BottomNavigationAction
						label="课程表"
						icon={<CalendarTodayIcon />}
						router="/classSchedule"
						onClick={this.clickHandle.bind(this)}
					/>
					<BottomNavigationAction
						label="校园卡"
						icon={<CreditCardIcon />}
						router="/schoolCard"
						onClick={this.clickHandle.bind(this)}
					/>
					<BottomNavigationAction
						label="我的"
						icon={<UserIcon />}
						router="/user"
						onClick={this.clickHandle.bind(this)}
					/>
				</BottomNavigation>
			</MuiThemeProvider>
		);
	}
}

SimpleBottomNavigation.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleBottomNavigation);