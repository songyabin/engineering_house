import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'

import style from './searc.module.less'
class SearchHeader extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
	}
	render() {
		return (
			<div className={style.search_header + " cf"}>

				<div className={style.hd_left + " fl"} onClick={this.clickHandle.bind(this)}>
					<i className="fa fa-angle-left"></i>
				</div>

				<div className={style.hd_right + " fr"}>
					搜索
				</div>

				<div className={style.hd_middle}>
					<div className={style.search_container + " cf"}>
						<i className="fa fa-search fl"></i>
						<input />
					</div>
				</div>
			</div>
		)
	}

	clickHandle() {
		window.history.back();
	}

	focusHandle() {

	}
}
export default SearchHeader