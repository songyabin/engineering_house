import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'

import { Link } from 'react-router-dom'
import style from './title.module.less'
class Title extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
	}
	render() {
		return (
			<div className={style.container + " cf"}>
				<div className={style.title + " fl"}>
					<i className="fa fa-tag"></i>
					<span>{this.props.title}</span>
				</div>
				{
					this.props.showMore
						? <div className={style.more + " fr"}>
							<Link to={this.props.linkto}>
								<span>更多</span>
								<i className="fa fa-angle-right"></i>
							</Link>
						</div>
						: ''
				}

			</div>
		)
	}
}
export default Title