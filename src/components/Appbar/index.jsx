import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';

const styles = {
	root: {
		flexGrow: 1,
		background: "#0069a0",
		color: "white",
		padding: 10,
	},
	text: {
		width: "100%",
		color: "white",
		fontSize: "16px",
		textAlign: "center",
	}
};

class HeaderTitle extends React.Component {
	render() {
		const { classes } = this.props;
		return (
			<AppBar position="static" color="primary" className={classes.root}>
				<Typography color="inherit" className={classes.text} >
					河南工程学院
          		</Typography>
			</AppBar>
		)
	}
}

HeaderTitle.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(HeaderTitle);