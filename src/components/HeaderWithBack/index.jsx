import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'

import style from './heade.module.less'
class HeaderWithBack extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
	}
	render() {
		return (
			<div className={style.wraper + " cf"}>
				<i className="fa fa-angle-left fa-2x fl" onClick={this.clickHandle.bind(this)}></i>
				<span className={style.title}>{this.props.title}</span>
			</div>
		)
	}

	clickHandle() {
		window.history.back();
	}
}
export default HeaderWithBack