import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import { Link } from 'react-router-dom'
import style from './style.module.less'
// import ItemAnnouncement from './item_homeAnnouncement'
// import ItemLostAndFound from './item_homeLostAndFound'
// import ItemList from './item_homeList'


class Item extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
	}
	render() {
		const data = this.props.data;
		return (
			<div className={style.list_item + " cf"}>
				<Link to={"/detail/" + data.id}>
					<div className={style.item_img_container + " fl"}>
						<img src={data.img} alt={data.title} />
					</div>
					<div className={style.item_content}>
						<div className={style.item_title_container + " cf"}>
							<h3 className="fl">{data.title}</h3>
							<span className="fr">{data.distance}m</span>
						</div>
						<p className={style.item_sub_title}>
							{data.subTitle}
						</p>
						<p className={style.item_price_container + " cf"}>
							<span className={style.price + " fl"}>￥{data.price}</span>
							<span className={style.area + " fr"}>{data.area}</span>
						</p>
					</div>
				</Link>
			</div>
		)
	}
}
export default Item