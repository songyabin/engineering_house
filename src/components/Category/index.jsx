import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import ReactSwipe from 'react-swipe';
import { Link } from 'react-router-dom'
import style from './categ.module.less'
class Category extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this);
		this.state = {
			index: 0
		}
	}
	render() {
		var opt = {
			auto: 2000,
			callback: function (index) {
				this.setState({ index })
			}.bind(this)
		}
		return (
			<div className={style.wraper}>
				<ReactSwipe className={style.carousel} swipeOptions={opt}>
					<div className={style.container}>
						<Link to="">
							<img src="http://www.liuxue86.com/uploadfile/images/20180112/a849b1d35acfea2ea6ae5d4317263a51.jpg" alt="" />
						</Link>
					</div>
					<div className={style.container}>
						<Link to="/search/all">
							<img src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1538655390729&di=5b73a3c1619c02d68384847fc6956333&imgtype=0&src=http%3A%2F%2Fimg.thupdi.com%2Fproject%2F2013%2F10%2F1393-f20dbeea-82eb-48bf-8804-5771099ed902.jpg" alt="" />
						</Link>
					</div>
					<div className={style.container}>
						<Link to="/search/all">
							<img src="https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1538655416887&di=ff1bd1fd4b22cb0b9e10369ad42f4396&imgtype=jpg&src=http%3A%2F%2Fimg2.imgtn.bdimg.com%2Fit%2Fu%3D387577072%2C3669953739%26fm%3D214%26gp%3D0.jpg" alt="" />
						</Link>
					</div>
					<div className={style.container}>
						<Link to="/search/all">
							<img src="http://imgsrc.baidu.com/forum/w%3D580/sign=bb12d2f48f025aafd3327ec3cbedab8d/c9a2032442a7d933a2947047a04bd11373f001d3.jpg" alt="" />
						</Link>
					</div>
					<div className={style.container}>
						<Link to="/search/all">
							<img src="http://imgsrc.baidu.com/forum/w%3D580/sign=a0ef70ccaa86c91708035231f93d70c6/600fa4d3fd1f4134a22f914a281f95cad1c85eff.jpg" alt="" />
						</Link>
					</div>
				</ReactSwipe>
				<div className={style.list_dots}>
					<ul className="cf">
						<li className={this.state.index === 0 ? style.active : ""}></li>
						<li className={this.state.index === 1 ? style.active : ""}></li>
						<li className={this.state.index === 2 ? style.active : ""}></li>
						<li className={this.state.index === 3 ? style.active : ""}></li>
						<li className={this.state.index === 4 ? style.active : ""}></li>
					</ul>
				</div>
			</div>
		)
	}
}
export default Category