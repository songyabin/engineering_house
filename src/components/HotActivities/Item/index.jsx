import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom'

const styles = {
	card: {
		border: "none",
		width: "100%",
		padding: 10,
	},
	actionArea: {
		width: "100%",
		padding: "10px 10px 0",
		border: "1px solid #ccc",
		borderRadius: '10px',
		boxShadow: "0px 1px 3px 0px rgba(0, 0, 0, 0.2), 0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 2px 1px -1px rgba(0, 0, 0, 0.12)"
	},
	media: {
		height: 120,
		overFlow: 'hidden'
	},
	content: {
		padding: "10px"
	}
};

function MediaCard(props) {
	const { classes } = props;
	const { data } = props
	return (
		<Link to={'/detail/' + data.id}>
			<Card className={classes.card}>
				<CardActionArea className={classes.actionArea}>
					<CardMedia
						className={classes.media}
						image={data.img}
						title={data.title}
					/>
					<CardContent className={classes.content}>
						<Typography gutterBottom variant="title" component="h5">{data.title}</Typography>
						<Typography component="p">{data.subTitle}</Typography>
					</CardContent>
				</CardActionArea>
			</Card>
		</Link>
	);
}

MediaCard.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MediaCard);