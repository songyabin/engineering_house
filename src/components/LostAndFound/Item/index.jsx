import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import { Link } from 'react-router-dom'
import style from './item.module.less'


class Item extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
	}
	render() {
		const data = this.props.data;
		return (
			<div className={style.wraper + " cf"}>
				<Link to={"/lostAndFound/" + data.id}>
					<div className={style.title_container}>
						{
							data.type
								? <span className={style.lost + " " + style.flag + " fl"}>寻</span>
								: <span className={style.found + " " + style.flag + " fl"}>领</span>
						}
						<div className={style.descri}>{data.descri}</div>
					</div>
					<div className={style.area_container + " cf"}>
						<span className="fl">{data.area}</span>
						<span className="fr">{data.time}</span>
					</div>
				</Link>
			</div>
		)
	}
}
export default Item