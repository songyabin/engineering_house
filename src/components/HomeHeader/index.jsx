import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import SearchInput from '../SearchInput'
import style from './homeh.module.less'
import createHistory from "history/createHashHistory"
const history = createHistory()
class NotFound extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
	}
	render() {
		return (
			<div className={style.home_header + " cf"}>
				<div className={style.search_container + " fl"}>
					<i className="fa fa-search"></i>
					<SearchInput value='' enterHandle={this.enterHandle.bind(this)} focusHandle={this.focusHandle.bind(this)} toSearchPage={true} />
				</div>
				<div className={style.icon_container + " fr"}>
					<i className="fa fa-plus"></i>
				</div>
			</div >
		)
	}
	enterHandle(value) {
		console.log('回车！！')
		// 敲回车处理事件，移动端暂时用不到
		// history.push('/search/all/' + encodeURIComponent(value))
	}

	focusHandle() {
		history.push('/search')
	}

}
export default NotFound