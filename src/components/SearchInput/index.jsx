import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'


import style from './searc.module.less'
class SearchInput extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
		this.state = {
			value: ''
		}
	}
	render() {
		return (
			<input
				type="text"
				placeholder="请输入关键字"
				className={style.search_input}
				value={this.state.value}
				onChange={this.changeHandle.bind(this)}
				onKeyUp={this.keyUpHandle.bind(this)}
				onFocus={this.focusHandle.bind(this)}
			/>
		)
	}

	componentDidMount() {
		this.setState({
			value: this.props.value || ''
		})
	}

	changeHandle(e) {
		this.setState({
			value: e.target.value
		})
	}

	keyUpHandle(e) {
		if (e.keyCode !== 13) {
			return
		}
		this.props.enterHandle(this.state.value)
	}

	focusHandle() {
		if (this.props.toSearchPage !== true) {
			return
		}
		this.props.focusHandle()
	}
}


export default SearchInput