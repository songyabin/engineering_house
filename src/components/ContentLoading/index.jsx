import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'

import style from './conte.module.less'
class ContentLoading extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
	}
	render() {
		return (
			<div className={style.wraper}>
				<span>正在拼命加载&nbsp;</span>
				<i className="fa fa-spinner fa-pulse"></i>
			</div>
		)
	}
}
export default ContentLoading