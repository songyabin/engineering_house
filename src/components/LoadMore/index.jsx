import React from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'

import style from './loadm.module.less'
class LoadMore extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.shouldComponentUpdate = PureRenderMixin.shouldComponentUpdate.bind(this)
	}
	render() {
		return (
			<div className={style.load_more} ref="wrapper">
				{
					this.props.isLoadingMore
						? <span>加载中...</span>
						: <span className={style.isLoading}>正在加载...</span>
				}
			</div>
		)
	}

	componentDidMount() {
		const loadMoreFn = this.props.loadMoreFn
		const wrapper = this.refs.wrapper
		let timeoutId
		function callback() {
			const top = wrapper.getBoundingClientRect().top
			const windowHeight = window.screen.height
			if (top && top < windowHeight) {
				// 当wrapper已经被滚动到暴露在页面可视范围之内的时候触发，加载更多数据
				loadMoreFn();
			}
		}
		window.addEventListener('scroll', function () {
			if (this.props.isLoadingMore) {
				return
			}
			// 截流，用户连续滚动时不触发下拉加载事件
			if (timeoutId) {
				clearTimeout(timeoutId)
			}
			timeoutId = setTimeout(callback, 50);
		}.bind(this), false)
	}

	loadMoreHandle() {
		// 执行传递过来的loadMoreData函数
		this.props.loadMoreFn()
	}
}
export default LoadMore