import React from 'react'
import {
	HashRouter,
	Route,
	Switch,
} from 'react-router-dom'

import App from '../containers'
import Home from '../containers/Home'
import Announcement from '../containers/Announcement'
import LostAndFound from '../containers/LostAndFound'
import Search from '../containers/Search'
import SchoolCard from '../containers/SchoolCard'
import Library from '../containers/Library'
import AcademicSystem from '../containers/AcademicSystem'
import ClassSchedule from '../containers/ClassSchedule'
import Postgraduate from '../containers/Postgraduate'
import Organization from '../containers/Organization'
import Market from '../containers/Market'
import MoreNav from '../containers/MoreNav'
import ActivityDetail from '../containers/ActivityDetail'
import HotActivities from '../containers/HotActivities'
import User from '../containers/User'
import NotFound from '../containers/404'
import Login from '../containers/Login'
import Test from '../test'
class RouteMap extends React.Component {
	render() {
		return (
			<HashRouter >
				<div>
					<App>
						<Switch>
							<Route exact path="/test" component={Test} />
							<Route exact path="/" component={Login} />
							<Route exact path="/login" component={Login} />
							<Route exact path="/home" component={Home} />
							<Route exact path="/schoolCard" component={SchoolCard} />
							<Route exact path="/library" component={Library} />
							<Route exact path="/academicSystem" component={AcademicSystem} />
							<Route exact path="/classSchedule" component={ClassSchedule} />
							<Route exact path="/postgraduate" component={Postgraduate} />
							<Route exact path="/organization" component={Organization} />
							<Route exact path="/market" component={Market} />
							<Route exact path="/moreNav" component={MoreNav} />
							<Route exact path="/search/:query?" component={Search} />
							<Route exact path="/lostAndFound/:id?" component={LostAndFound} />
							<Route exact path="/hotActivities" component={HotActivities} />
							<Route exact path="/detail/:id" component={ActivityDetail} />
							{/* <Route path="/login/:router?" component={Login} /> */}
							<Route exact path="/user" component={User} />
							{/* <Route exact path="/search/:type/:keyword?" component={Search} /> */}
							<Route exact path="/announcement/:id?" component={Announcement} />
							<Route path="*" component={NotFound} />
						</Switch>
					</App>
				</div>
			</HashRouter>
		)
	}
}


export default RouteMap